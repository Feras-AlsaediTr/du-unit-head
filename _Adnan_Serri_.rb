class BirthdayMessagesController < ApplicationController
  def print_message
    birthday_message = "Dear Dr. Ubai Sandouk,\n\n"\
                       "On your special day, we pause to celebrate not only your birthday but also your immense contributions to our development team. "\
                       "Your dedication to innovation has been invaluable in driving our unit towards success.\n\n"\
                       "Under your guidance, we have grown both individually and collectively, reaching new heights in our projects. "\
                       "Your passion for learning and your ability to inspire us have created an environment of continuous growth and achievement.\n\n"\
                       "Today, we honor not only your accomplishments but also the leader and mentor you are. Happy Birthday, Dr. Ubai Sandouk!\n\n"\
                       "Warm regards,\nAdnan Serri"

    puts birthday_message
    render plain: birthday_message
  end
end
