def wish_birthday(name):
  """
  This function takes a name as input and returns a birthday wish message.

  Args:
    name (str): The name of the person to wish a birthday to.

  Returns:
    str: A birthday wish message.
  """
  message = f"🎉 Happy birthday, {name}! 🎂 I hope you have a wonderful day filled with joy 🎁🎉"
  return message

if __name__ == "__main__":
  name = "Dr. Ubai"
  birthday_wish = wish_birthday(name)
  print(birthday_wish)
