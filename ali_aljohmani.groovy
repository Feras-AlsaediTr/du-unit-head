pipeline {
    agent any
    
    stages {
        stage('Celebrate Dr. Ubai Birthday') {
            steps {
                script {
                    def celebrateBirthday = { String name ->
                        echo "Happy Birthday to you $name!"
                        echo "Wishing you a year filled with happiness and success $name!"
                        echo "Celebrating you with utmost admiration and respect."
                    }
                    
                    celebrateBirthday("Dr. Ubai")
                }
            }
        }
    }
}
