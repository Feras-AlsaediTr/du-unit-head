
abstract class  Birthday{
  wish();
}

class DrUbaiBirthday extends Birthday{
  @override
  wish() {
   print('''Happy birthday! 🎉🎂🎈
       On this special day,
        I want to send you my warmest wishes for a fantastic birthday filled with joy,love,health ''');
  }

}


void main(){
  DrUbaiBirthday drUbaiBirthday=new DrUbaiBirthday();
  drUbaiBirthday.wish();

}
