public class AhmadAbdullhay {

    public static void main(String[] args) {
        HappyBirthday happyBirthday = new WhyJava();
        happyBirthday.toDrUbai();
    }
}

abstract class HappyBirthday {
    void toDrUbai() {
        System.out.println("Happy Birthday Dr Ubai <3");
    }
}

abstract class AWish extends HappyBirthday {
    void toDrUbai() {
        super.toDrUbai();
        System.out.println("Wish you all the best, And a happy new year full of joy and success");
    }
}
abstract class DrUbaiEffect extends  AWish {
    @Override
    void toDrUbai() {
        super.toDrUbai();
        System.out.println("With you we all push forward, And explore areas we never even imagined!");
    }
}

class WhyJava extends  DrUbaiEffect {
    @Override
    void toDrUbai() {
        super.toDrUbai();
        System.out.println("I choose Java because I learned it in Programming Language course taught by you");
        System.out.println("I consider this course as my real start on this field, And I will be always grateful");
    }
}