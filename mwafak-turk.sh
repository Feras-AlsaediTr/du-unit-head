#!/bin/bash

send_birthday_wishes() {
    echo "╭━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╮"
    echo "│  🎉🎂 Happy Birthday, Dr. Ubai! 🎂🎉  │"
    echo "╰━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━╯"
    echo ""
    echo "🎉 May your day be filled with happiness, inspiration, and memorable moments!"
    echo "🌟 May this special day be as outstanding as your contributions to our unit!"
    echo ""
    echo "Best regards,"
    echo "Mwafak Turk"
}

# Main script
clear
echo "🎉🎂 Welcome to the Birthday Wishing Script! 🎂🎉"
echo ""
echo "Sending birthday wishes to Dr. Ubai..."
sleep 2 

send_birthday_wishes
